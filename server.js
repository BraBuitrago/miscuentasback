//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

//movimientos
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/bbuitrago/collections/";
var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bbuitrago/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlcuentasMLab = "https://api.mlab.com/api/1/databases/bbuitrago/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urltransferenciasMLab = "https://api.mlab.com/api/1/databases/bbuitrago/collections/transferencias?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var urlusuariosMLab = "https://api.mlab.com/api/1/databases/bbuitrago/collections/usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
//var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bbuitrago/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&s={'apellido':1}&f={'nombre':1,'apellido':1}&q={'nombre':'Paco'}&l=1&c=false";
var clienteMLabMovimientos = requestjson.createClient(urlmovimientosMLab);
var clienteMLabCuentas = requestjson.createClient(urlcuentasMLab);
var clienteMLabTransferencias = requestjson.createClient(urltransferenciasMLab);
var clienteMLabUsuarios = requestjson.createClient(urlusuariosMLab);


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res){
  res.sendfile(path.join(__dirname, 'index.html')); //__dirname es la ruta raiz y despues el archivo

});

app.get('/usuarios/:idusuario', function(req, res){ // :idcliente es el parametro que recibe. Trae los datos del Usuario
  var urlusuario = urlusuariosMLab + "&q={'identificacion':'" + req.params.idusuario + "'}"
  var clienteUsuario = requestjson.createClient(urlusuario);
  clienteUsuario.get('', function(err, resM, body) {
    if (err) {
      console.log(body);
    }  else {
      res.send(body);
    }
  })
})

app.get('/cuentas/:idusuario', function(req,res){ // Traer las cuentas de un usuario
  var urlcuentas = urlcuentasMLab + "&q={'identificacion':'" + req.params.idusuario + "'}&f={'numero_cuenta':1,'_id':0}"
  var clienteCuentas = requestjson.createClient(urlcuentas);
  clienteCuentas.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/monto/:idcuenta', function(req,res){ // Traer las cuentas de un usuario
  var urlcuentas = urlcuentasMLab + "&q={'numero_cuenta':" + req.params.idcuenta + "}&f={'monto':1,'_id':0}"
  var clienteMonto = requestjson.createClient(urlcuentas);
  clienteMonto.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/transferencias/:idcuenta', function(req,res){
  var urltransferencias = urltransferenciasMLab + "&q={'cuenta_afectada':" + req.params.idcuenta + "}&s={'fecha_trans':-1}&f={'_id':0,  'cuenta_relacionada':1, 'monto_trans':1,  'fecha_trans':1}"
  var clienteTransferencias = requestjson.createClient(urltransferencias);
  clienteTransferencias.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/password/:idusuario', function(req,res){
  var urlusuarios = urlusuariosMLab  + "&q={'identificacion':'" + req.params.idusuario +  "'}&f={'password':1,'_id':0}"
  var clientePass = requestjson.createClient(urlusuarios);
  clientePass.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/usuarios', function(req,res){
  clienteMLabUsuarios.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/nocuentas/:idusuario', function(req,res){
  var urlnocuentas = urlcuentasMLab + "&q={'identificacion':{$ne:'" + req.params.idusuario + "'}}&f={'numero_cuenta':1,'_id':0}"
  //&q={"identificacion":{$ne: "1018454013"}}&f={"numero cuenta":1,"_id":0}
  var clienteNoCuentas = requestjson.createClient(urlnocuentas);
  clienteNoCuentas.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
    //console.log(body);
  }
})
})

app.get('/movimientos', function(req,res){
  clienteMLabMovimientos.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

app.get('/ultimacuenta', function(req,res){
  var urlultimacuenta = urlcuentasMLab + "&s={'numero_cuenta':-1}&f={'_id':0, 'numero_cuenta':1}&l=1"
  //&s={"numero_cuenta":-1}&f={"_id":0, "numero_cuenta":1}&l=1
  var clienteUltimaCuenta = requestjson.createClient(urlultimacuenta);
  clienteUltimaCuenta.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})

var movimientosJSON = require('./movimientosv1.json');

app.get('/v1/movimientos/',function(req,res){
  //res.send('movimientosv1.json');
  res.send(movimientosJSON);
})



app.post('/montocuenta/:idcuenta', function(req, res) {
 var clienteMlab = requestjson.createClient(urlMLabRaiz + 'cuentas')
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
 console.log(cambio);
 clienteMlab.put('?q={"numero_cuenta":' + req.params.idcuenta + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
   res.send(body)
 })
})

app.delete('/borrarcuenta/:idcuenta', function(req,res){
  var clienteMlab = requestjson.createClient(urlMLabRaiz + 'cuentas/')
  console.log(clienteMlab);
  clienteMlab.get(req.params.idcuenta + '?' + apiKey, function(err, resM) {
    res.send(clienteMlab);
  })
})

app.get('/movimientos', function(req,res){
  clienteMLabMovimientos.get('', function(err, resM, body) {
  if (err) {
    console.log(body);
  }  else {
    res.send(body);
  }
})
})


app.post('/movimientos', function(req, res) {
  clienteMLabMovimientos.post('', req.body,  function(err, resM, body) {
    res.send(body);
    console.log(body);
})
})

app.post('/transferencias', function(req, res) {
  clienteMLabTransferencias.post('', req.body,  function(err, resM, body) {
    res.send(body);
})
})

app.post('/cuentas', function(req, res) {
  clienteMLabCuentas.post('', req.body,  function(err, resM, body) {
    res.send(body);
})
})

app.post('/usuarios', function(req, res) {
  clienteMLabUsuarios.post('', req.body,  function(err, resM, body) {
    res.send(body);
})
})
